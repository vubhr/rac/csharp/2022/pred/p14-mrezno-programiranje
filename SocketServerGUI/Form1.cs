﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace SocketServerGUI {
  public partial class Form1 : Form {
    public Form1() {
      InitializeComponent();
      server = new TcpListener(IPAddress.Any, 50000);
    }

    private void slusajPoruke() {
      while (true) {
        TcpClient klijent = server.AcceptTcpClient();
        NetworkStream stream = klijent.GetStream();
        byte[] poruka = new byte[1024];
        stream.Read(poruka, 0, 100);
        string porukaString = Encoding.ASCII.GetString(poruka).TrimEnd('\0');
        lbPoruke.Items.Add(DateTime.Now + " - " + porukaString);
        klijent.Close();
      }
    }

    private void pokreniServer() {
      server.Start();
      Task.Run(() => {
        slusajPoruke();
      });
    }

    private void Form1_Load(object sender, EventArgs e) {
      pokreniServer();
    }

    private TcpListener server;
  }
}
